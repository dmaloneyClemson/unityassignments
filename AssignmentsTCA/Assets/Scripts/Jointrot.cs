﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jointrot : MonoBehaviour
{
    public GameObject Joint1;
    public Vector3 eulerAngles;
    private MeshFilter mf;
    private Vector3[] origVerts;
    private Vector3[] newVerts;

    void Start()
    {
        mf = GetComponent<MeshFilter>();
        origVerts = mf.mesh.vertices;
        newVerts = new Vector3[origVerts.Length];
    }

    // Update is called once per frame
    void Update()
    {
        Quaternion rotation = Quaternion.Euler(eulerAngles.x, eulerAngles.y, eulerAngles.z);
        Matrix4x4 m = Matrix4x4.Rotate(rotation);
        int i = 0;
        while (i < origVerts.Length) {
            newVerts[i] = m.MultiplyPoint3x4(origVerts[i]);
            i++;
        }
        mf.mesh.vertices = newVerts;
         if (Input.GetKey(KeyCode.Keypad1))
        {
            transform.Rotate(Vector3.right, Time.deltaTime*-10);        
        }
         if (Input.GetKey(KeyCode.Keypad2))
        {
            transform.Rotate(Vector3.up, Time.deltaTime*10);        
        }

    }
}
