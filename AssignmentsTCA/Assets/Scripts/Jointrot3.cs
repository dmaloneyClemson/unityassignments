﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jointrot3 : MonoBehaviour
{
    public GameObject Joint3;
    // Start is called before the first frame update
    public Vector3 eulerAngles;
    private MeshFilter mf;
    private Vector3[] origVerts;
    private Vector3[] newVerts;

    void Start()
    {
        mf = GetComponent<MeshFilter>();
        origVerts = mf.mesh.vertices;
        newVerts = new Vector3[origVerts.Length];
    }

    // Update is called once per frame
    void Update()
    {
        Quaternion rotation = Quaternion.Euler(eulerAngles.x, eulerAngles.y, eulerAngles.z);
        Matrix4x4 m = Matrix4x4.Rotate(rotation);
        int i = 0;
        while (i < origVerts.Length) {
            newVerts[i] = m.MultiplyPoint3x4(origVerts[i]);
            i++;
        }
         if (Input.GetKey(KeyCode.Keypad7))
        {
            transform.Rotate(Vector3.right, Time.deltaTime*-10);        
        }
         if (Input.GetKey(KeyCode.Keypad8))
        {
            transform.Rotate(Vector3.up, Time.deltaTime*10);        
        }
    }
}